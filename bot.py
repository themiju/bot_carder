#!/usr/bin/env python

import json
import logging

from telegram import Bot, Update

from config import config
from controllers import build_buttons_menu
from controllers.Card import add_card, list_cards, show_card, edit_card, \
    delete_card
from controllers.Menu import main_menu

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO, BytesIO
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, \
    MessageHandler, Filters

AVAILABLE_ACTIONS = {
    'list_cards': list_cards,
    'show_card': show_card,
    'edit_card': edit_card,
    'delete_card': delete_card,
    'add_card': add_card,
    'main_menu': main_menu,
}


def debug(bot, update):
    pass
    # button_list = [
    #     InlineKeyboardButton("col11", callback_data='some data #1'),
    #     InlineKeyboardButton("col12", callback_data='some data #2'),
    #     InlineKeyboardButton("col21", callback_data='some data #3'),
    #     InlineKeyboardButton("col22", callback_data='some data #3'),
    # ]
    # reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=2))
    # bot.send_message(chat_id=update.message.chat_id,
    #                  text="Choose one",
    #                  # parse_mode=telegram.ParseMode.MARKDOWN,
    #                  reply_markup=reply_markup)
    bot.send_message(chat_id=update.message.chat_id,
                     text="/show_card 67")


def callback_handler(bot: Bot, update: Update, user=None):
    try:
        data = json.loads(update.callback_query.data)
    except AttributeError:
        data = {}
    action = data.get('action')
    if action in AVAILABLE_ACTIONS:
        AVAILABLE_ACTIONS[action](bot=bot, update=update, data=data)


fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=config.get('logging_level'), format=fmt)
logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(config.get('logging_level'))

updater = Updater(token=config['telegram_token'])

updater.dispatcher.add_handler(CommandHandler('start', main_menu))
updater.dispatcher.add_handler(CommandHandler('add_card', add_card))
updater.dispatcher.add_handler(CommandHandler('list_cards', list_cards))
updater.dispatcher.add_handler(CommandHandler('show_card', show_card))
updater.dispatcher.add_handler(CommandHandler('debug', debug))
updater.dispatcher.add_handler(CallbackQueryHandler(callback=callback_handler))
updater.dispatcher.add_handler(MessageHandler(Filters.text, list_cards))

updater.start_polling()
updater.idle()
