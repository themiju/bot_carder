import logging
import os
from os.path import join, dirname
from os import environ as env

tg_token = env.get('CRD_TG_TOKEN')
if not tg_token:
    raise Exception('Telegram token is missing')

storage_directory = os.path.join(os.path.dirname(__file__), 'storage')
logging_level = env.get('LOGGING_LEVEL', logging.DEBUG)

config = {
    'telegram_token': tg_token,
    'storage_directory': storage_directory,
    'logging_level': logging_level,
    'db_type': env.get('CRD_DB_TYPE', 'sqlite'),  # postgresql, mysql
    'db_name': env.get('CRD_DB_NAME', 'carder'),
    'db_host': env.get('CRD_DB_HOST', '127.0.0.1'),
    'db_user': env.get('CRD_DB_USER', 'carder'),
    'db_pass': env.get('CRD_DB_PASS', 'carder'),
    'db_port': env.get('CRD_DB_PORT', 3306),
    'db_path': env.get('CRD_DB_PATH', join(dirname(__file__), 'crd.sqlite')),
}
