from json import dumps

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, \
    ReplyKeyboardMarkup

COLUMN_NUMBER = 3


def build_menu(button_list, cols=COLUMN_NUMBER):
    buttons = [button_list[i:i + cols] for i in
               range(0, len(button_list), cols)]
    return buttons


def build_buttons_menu(button_list, cols=COLUMN_NUMBER, reply=False):
    btns = [InlineKeyboardButton(b['title'], callback_data=dumps(b['data']))
            for b in button_list]

    if reply:
        markup = ReplyKeyboardMarkup(build_menu(btns, cols), True, True)
    else:
        markup = InlineKeyboardMarkup(build_menu(btns, cols))

    return markup
