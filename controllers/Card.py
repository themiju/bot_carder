#!/usr/bin/env python

import barcode
import telegram
from telegram import Bot, Update

from decorators import handle_user, handle_card
from . import build_buttons_menu

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO, BytesIO
from barcode.writer import ImageWriter

from models.Card import Card
from models.File import File
from models.User import User, DoesNotExist


def _get_text(message):
    text = None
    if hasattr(message, 'entities'):
        for entity in message.entities:
            if entity.BOT_COMMAND == entity.type:
                text = [term.strip() for term in message.text.split(' ')]

    return text


@handle_user
def add_card(bot: Bot, update: Update, user: User, *args, **kwargs):
    """
    If this method is called as pressed buttons callback then send a message and
    quit.
    """
    data = _get_text(update.message)

    if (data is None) or (3 > len(data)) or ('data' in kwargs):
        bot.send_message(chat_id=update.effective_message.chat.id,
                         text='Use a code like this: `/add_card 2128 Ikea`',
                         parse_mode=telegram.ParseMode.MARKDOWN,
                         reply_markup=build_buttons_menu([
                             {'title': "Main menu",
                              'data': {'action': 'main_menu'}},
                         ]))
        return True

    card_code = data[1]
    card_title = data[2]

    try:
        card_obj = Card.select() \
            .where(Card.code == card_code) \
            .where(Card.user == user) \
            .get()
    except DoesNotExist:
        card_obj = Card(user=user, code=card_code, title=card_title)
        card_obj.save()

        fp = BytesIO()
        ean = barcode.codex.Code39(card_code, writer=ImageWriter(),
                                   add_checksum=False)
        ean.write(fp)
        card_obj.add_file(fp)

    file_obj = card_obj.files.where(File.type == File.TYPE_CODE).get()

    bot.send_photo(
        chat_id=update.message.chat.id,
        photo=open(file_obj.abs_path, 'rb'),
        reply_to_message_id=update.effective_message.message_id,
        reply_markup=build_buttons_menu([
            {'title': "Edit card",
             'data': {'action': 'edit_card', 'card': card_obj.id}},
            {'title': "Main menu", 'data': {'action': 'main_menu'}},
        ])
    )


@handle_user
def list_cards(bot: Bot, update: Update, user: User, *args, **kwargs):
    message = update.effective_message
    cards = user.cards
    if update.callback_query is None and message.text and not message.entities:
        wildcard = '%' + message.text + '%'
        cards = cards.where(Card.title ** wildcard)
    if cards:
        text = "Here is your cards"
        button_list = [{
            'title': c.title or '<Unnamed card>',
            'data': {'action': 'show_card', 'card': c.id, }
        } for c in cards]
        bot.send_message(chat_id=message.chat_id,
                         text=text,
                         reply_markup=build_buttons_menu(button_list))
    else:
        text = "Can't find any cards"
        bot.send_message(chat_id=message.chat.id,
                         text=text)


@handle_card
def show_card(bot: Bot, update: Update, data, user: User, card: Card):
    message = update.effective_message

    file_obj = card.files.where(File.type == File.TYPE_CODE).get()
    bot.send_photo(
        chat_id=message.chat.id,
        caption=card.title,
        photo=open(file_obj.abs_path, 'rb'),
        reply_markup=build_buttons_menu([
            {'title': "Edit card",
             'data': {'action': 'edit_card', 'card': card.id}},
            {'title': "Main menu", 'data': {'action': 'main_menu'}},
        ])
    )


@handle_card
def edit_card(bot: Bot, update: Update, data, user: User, card: Card):
    message = update.effective_message

    bot.send_message(chat_id=message.chat.id,
                     text="Available actions for %s" % card.title,
                     reply_markup=build_buttons_menu([
                         {'title': "Delete",
                          'data': {'action': 'delete_card', 'card': card.id}},
                         {'title': "Main menu",
                          'data': {'action': 'main_menu'}},
                     ]))


@handle_card
def delete_card(bot: Bot, update: Update, data: dict, user: User, card: Card):
    message = update.effective_message

    if data.get('confirmed', False):
        card.delete_instance()
        bot.send_message(chat_id=message.chat.id,
                         text="Successfully deleted %s" % card.title,
                         reply_markup=build_buttons_menu([
                             {'title': "Main menu",
                              'data': {'action': 'main_menu'}},
                         ]))
    else:
        bot.send_message(chat_id=message.chat.id,
                         text="Are you sure you want to delete %s" % card.title,
                         reply_markup=build_buttons_menu([
                             {'title': "Yes",
                              'data': {'action': 'delete_card', 'card': card.id,
                                       'confirmed': True, }},
                             {'title': "Main menu",
                              'data': {'action': 'main_menu'}},
                         ]))
