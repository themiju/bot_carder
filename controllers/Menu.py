#!/usr/bin/env python
import json
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Bot, Update

from controllers import build_buttons_menu
from decorators import handle_user
from models import User


@handle_user
def main_menu(bot: Bot, update: Update, user: User, *args,  **kwargs):
    message = update.effective_message

    bot.send_message(chat_id=message.chat.id,
                     text="Main menu",
                     reply_markup=build_buttons_menu([
                         {'title': "Add card", 'data': {'action': 'add_card'}},
                         {'title': "List cards", 'data': {'action': 'list_cards'}},
                     ]))
