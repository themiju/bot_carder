import os
from peewee import *
import pathlib

from config import config
from models import BasicModel


class File(BasicModel):
    path = CharField()
    type = IntegerField()
    card = DeferredForeignKey('Card', backref='files')

    TYPE_CODE = 0
    TYPE_OTHER = 1

    @property
    def abs_path(self):
        p = pathlib.Path(config.get('storage_directory'), self.path)
        p = p.resolve().as_posix()

        return p

    class Meta:
        table_name = 'files'
