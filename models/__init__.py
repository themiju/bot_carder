from peewee import Model, MySQLDatabase, PostgresqlDatabase, SqliteDatabase

from config import config

db = None
if 'mysql' == config['db_type']:
    db = MySQLDatabase(config['db_name'], user=config['db_user'],
                       password=config['db_pass'],
                       host=config['db_host'],
                       port=config['db_port'])
elif 'postgresql' == config['db_type']:
    db = PostgresqlDatabase(config['db_name'], user=config['db_user'],
                            password=config['db_pass'],
                            host=config['db_host'],
                            port=config['db_port'])
else:
    db = SqliteDatabase(config['db_path'])

if db is None:
    raise Exception('db is not set')

db.connect()


class BasicModel(Model):
    class Meta:
        database = db
