from peewee import *

from models import BasicModel


class User(BasicModel):
    telegram_id = IntegerField()

    class Meta:
        table_name = 'users'

    def __repr__(self) -> str:
        return 'User "%d" (tg id %d)' % (self.id, self.telegram_id)
