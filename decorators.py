#!/usr/bin/env python
from functools import wraps

from peewee import DoesNotExist
from telegram import Bot, Update

from controllers import build_buttons_menu
from models.Card import Card
from models.User import User


def handle_user(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        try:
            user = User.get(User.telegram_id == update.effective_user.id)
        except DoesNotExist:
            user = User()
            user.telegram_id = update.effective_user.id
            user.save()
        return func(bot, update, user=user, *args, **kwargs)

    return wrapped


def handle_card(func):
    @wraps(func)
    @handle_user
    def wrapped(bot: Bot, update: Update, user: User=None, data=None, *args, **kwargs):
        message = update.effective_message

        try:
            card = user.cards.select().where(
                Card.id == data.get('card', -1)).get()
            return func(bot, update, data=data, user=user, card=card, *args, **kwargs)
        except DoesNotExist:
            bot.send_message(chat_id=message.chat.id,
                             text="Can't find this card",
                             reply_markup=build_buttons_menu([
                                 {'title': "List cards",
                                  'data': {'action': 'list_cards'}},
                             ]))
            return True

    return wrapped
