certifi==2018.4.16
future==0.16.0
peewee==3.4.0
Pillow==5.1.0
pyBarcode==0.7
python-telegram-bot==10.1.0
